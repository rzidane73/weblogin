<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/create-mahasiswa', 'HomeController@create')->name('create-mahasiswa');
Route::post('/simpan-mahasiswa', 'HomeController@store')->name('simpan-mahasiswa');
Route::get('/edit-mahasiswa/{id}', 'HomeController@edit')->name('edit-mahasiswa');
Route::post('/update-mahasiswa/{id}', 'HomeController@update')->name('update-mahasiswa');
Route::get('/delete-mahasiswa/{id}', 'HomeController@destroy')->name('delete-mahasiswa');

Auth::routes();