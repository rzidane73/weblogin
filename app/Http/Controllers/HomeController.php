<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\Jurusan;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $mhs = Mahasiswa::all();
        return view('home', compact('mhs'));
    }

    public function create()
    {
        $jur = Jurusan::all();
        return view('create-mahasiswa', compact('jur'));
    }

    public function store(Request $request)
    {
        // dd($request->all()); //

        Mahasiswa::create([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'email' => $request->email,
            'jurusan_id' => $request->jurusan_id,
        ]);

        return redirect('home')->with('success', 'Data Berhasil Tersimpan');
    }

    public function edit($id)
    {
        $jur = Jurusan::all();
        $maha = Mahasiswa::findorfail($id);
        return view('edit-mahasiswa', compact('maha', 'jur'));
    }

    public function update(Request $request, $id)
    {
        $maha = Mahasiswa::findorfail($id);
        $maha->update($request->all());
        return redirect('home')->with('success', 'Data Berhasil Diubah');
    }

    public function destroy($id)
    {
        $maha = Mahasiswa::findorfail($id);
        $maha->delete();
        return back()->with('info', 'Data Berhasil Dihapus');
    }
}
