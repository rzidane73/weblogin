@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-13">
            <center><h1 class="mt-1">Universitas Brawijaya</h1></center>
            <br>
            <br>
            
            <h1 class="mt-1">Daftar Mahasiswa</h1>
            
            <table class="table">
                <thead class="table-dark">
                <tr>
                
                  <th scope="col">Nama</th>
                  <th scope="col">NIM</th>
                  <th scope="col">Email</th>
                  <th scope="col">Jurusan</th>
                  <th scope="col">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($mhs as $item)

                    <tr>
                        
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->nim }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->jurusan->jurusan }}</td>
                        <td>
                            <a href="{{ url('edit-mahasiswa', $item->id) }}" class="badge bg-info text-dark">edit</a>
                            <a href="{{ url('delete-mahasiswa', $item->id) }}" class="badge bg-danger text-dark">delete</a>
                            
                        </td>
                    </tr>
                  @endforeach

                        </tbody>
                      </table>

                      
                    <center><a href="{{ route('create-mahasiswa') }}" class="btn btn-success">Tambah</a></center>
                              
        </div>
    </div>
</div>

@include('sweetalert::alert')

@endsection
