@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            
            
            <h1 class="mt-1">Form Mahasiswa Brawijaya</h1>
            
            <div class="card-body">
                <form action="{{ route('simpan-mahasiswa') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Mahasiswa">
                    </div>

                    <div class="form-group">
                        <select class="form-control" style="width: 100%;" name="jurusan_id" id="jurusan_id">
                        <option disabled value>Pilih Jurusan</option>
                        @foreach ( $jur as $item )
                            <option value="{{ $item->id }}">{{ $item->jurusan }}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" id="nim" name="nim" class="form-control" placeholder="NIM">
                    </div>

                    <div class="form-group">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Simpan Data</button>
                    </div>
                </form>
            </div>
                              
        </div>
    </div>
</div>
@endsection
